package turing;
import java.util.Scanner;
public class Main {
    private String[] movieNames;
    private double[] movieScores;
    private int numMovies;
    public Main() {
        movieNames = new String[3];
        movieScores = new double[3];
        numMovies = 0;
    }
    public static void main(String[] args) {
        Main menu = new Main();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nSelect an option:");
            System.out.println("1. Input movies");
            System.out.println("2. Display movies and ratings");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    menu.inputMovies(scanner);
                    break;
                case 2:
                    menu.displayMovies();
                    break;
                case 0:
                    System.out.println("Exiting...");
                    scanner.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    public void inputMovies(Scanner scanner) {
        if (numMovies >= 3) {
            System.out.println("You have already input 3 movies. Please display them instead.");
            return;
        }

        for (int i = 0; i < 3; i++) {
            System.out.print("Enter movie name: ");
            movieNames[numMovies] = scanner.next();
            System.out.print("Enter movie score: ");
            movieScores[numMovies] = scanner.nextDouble();
            numMovies++;
        }
    }

    public void displayMovies() {
        if (numMovies == 0) {
            System.out.println("No movies have been input yet.");
            return;
        }

        System.out.println("\nList of movies and their ratings:");
        for (int i = 0; i < numMovies; i++) {
            System.out.printf("%s: %.1f%n", movieNames[i], movieScores[i]);
        }
    }
}
