package turing.les2;
import java.util.Scanner;
public class e8629 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = a/1000;
        int c = (a%1000)/100;
        int d = (a%100)/10;
        int e = a%10;
        if ( b%2 == 1 || c%2 == 1 || d%2 == 1 || e%2 == 1 ) System.out.println("YES");
        else System.out.println("NO");
    }
}
