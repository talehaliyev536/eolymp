package turing.les2;
import java.util.Scanner;
public class e8894 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
    if ( n > 0 && n%2 == 1 ) System.out.println("YES");
    else if ( n < 0 && n%2 == 0) System.out.println("YES");
    else System.out.println("NO");
    }
}
