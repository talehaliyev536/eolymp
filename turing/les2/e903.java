package turing.les2;

import java.util.Scanner;

public class e903 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = a/100;
        int c = a%10;
        if ( b > c ) System.out.println(b);
        else if ( b < c) System.out.println(c);
        else if ( b == c) System.out.println("=");
    }
}
