package turing.les2;
import java.util.Scanner;
public class e8893 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if ( n%3 == 0 && n%2 == 0 && ( ( n > -100 && n < -9 ) || ( n > 9 && n < 100 ) ) ) {
            System.out.println("YES");
        }
        else System.out.println("NO");
    }
}
