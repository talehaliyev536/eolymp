package turing.les2;
import java.util.Scanner;
public class e905 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        if ( a == b && a == c) System.out.println(1);
        else if ( a==b && a!=c || a==c && c!=b || b==c && a!=b) System.out.println(2);
        else if ( a != c && a!= b) System.out.println(3);
    }
}
