package turing.les2;
import java.util.Scanner;
public class e8626 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        if ( (a/1000) == 3 && (a%1000)/100 == 7 ) System.out.println("YES");
        else if ( (a%1000)/100 == 3 && (a%100)/10 == 7) System.out.println("YES");
        else if ( (a%100)/10 == 3 && a%10 == 7 ) System.out.println("YES");
        else System.out.println("NO");
    }
}
