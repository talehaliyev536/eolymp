package turing.les2;
import java.util.Scanner;
public class e2042 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int Month = in.nextInt();
        if ( Month >= 1 && Month <= 3) System.out.println("First");
        else if ( Month >=4 && Month<= 6) System.out.println("Second");
        else if ( Month >=7 && Month <= 9) System.out.println("Third");
        else if ( Month >=10 && Month <=12) System.out.println("Fourth");
    }
}
