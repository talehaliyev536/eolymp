package turing.les2;
import java.util.Scanner;
public class e923 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int Season = in.nextInt();
        if ( Season >= 3 && Season <= 5) System.out.println("Spring");
        else if ( Season >=6 && Season <= 8) System.out.println("Summer");
        else if ( Season >=9 && Season <= 11) System.out.println("Autumn");
        else if ( Season ==12 || Season == 1 || Season == 2 ) System.out.println("Winter");
    }
}
