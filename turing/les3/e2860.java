package turing.les3;
import java.util.Scanner;
public class e2860 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int sum = 0;
        if ( b > a ) {
            for (int i = a; i <= b; i++) {
                sum += i;
            }
            System.out.println(sum);
        } else if ( a > b ) {
            for (int i = b; i <= a; i++) {
                sum += i;
            }
            System.out.println(sum);
        }
    }
}