package turing.les3;
import java.util.Scanner;
public class e0622 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int z = 0;
        while (n != 0 ){
            if ( n%2 == 1) {
                z++;
            }
            n /= 2;
        }
        System.out.println(z);
    }
}