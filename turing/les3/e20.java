package turing.les3;
import java.util.Scanner;
public class e20{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = a , sum = 0 , num = 0;
        while ( a > 0 ) {
            while ( b > 0 ) {
                sum += (b % 10);
                b /= 10;
            }
            num++;
            a -= sum;
            sum = 0;
            b = a;
        }
        System.out.println(num);
    }
}

