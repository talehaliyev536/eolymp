package turing.eolymp;

import java.util.Scanner;

public class e8605 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float x = in.nextFloat();
        float y = in.nextFloat();
        float z = in.nextFloat();
        System.out.println((x+y)+" "+(x+z)+" "+(y+z));
    }
}
