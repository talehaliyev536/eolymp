package turing.eolymp;

import java.util.Scanner;

public class e8611 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long d = in.nextLong();
        if (d > 0) {
            System.out.println("Water");
        }
        else {
            System.out.println("Ice");
        }
    }
}
