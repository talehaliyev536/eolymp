package turing.eolymp;

import java.util.Scanner;

public class e8603 {
    public static void main(String[] args) {
        Scanner m = new Scanner(System.in);
        int a = m.nextInt();
        int b = a / 100 + ((a % 100) / 10) + (a % 10);
        int c = a / 100 * ((a % 100) / 10) * (a % 10);
        System.out.print(b + " " + c);
    }
}
