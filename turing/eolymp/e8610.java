package turing.eolymp;
import java.util.Scanner;
public class e8610 {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            String n = in.nextLine();
            char a = n.charAt(0);
            int b = (int) a;
            int c = b - 1;
            int d = b + 1;
            System.out.println((char) c + " " + (char) d);
        }
}